<?php
/**
 * Register settings
 *
 * @package     WidgitLabs\SiteRipper\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function siteripper_add_menu( $menu ) {
	$menu['type']       = 'menu';
	$menu['page_title'] = __( 'SiteRipper', 'siteripper' );
	$menu['menu_title'] = __( 'SiteRipper', 'siteripper' );
	$menu['icon']       = 'none';
	$menu['show_title'] = true;

	return $menu;
}
add_filter( 'siteripper_menu', 'siteripper_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function siteripper_settings_tabs( $tabs ) {
	$tabs['ripper']   = __( 'Ripper', 'siteripper' );
	$tabs['settings'] = __( 'Settings', 'siteripper' );
	$tabs['support']  = __( 'Support', 'siteripper' );

	return $tabs;
}
add_filter( 'siteripper_settings_tabs', 'siteripper_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function siteripper_registered_settings_sections( $sections ) {
	// TODO: Make Welcome! only show on initial install and updates.
	$sections = array(
		'ripper' => array(
			'dashboard' => __( 'Dashboard', 'siteripper' ),
			'ripper'    => __( 'Ripper', 'siteripper' ),
		),
		'settings'  => array(
			'segments' => __( 'Segments', 'siteripper' ),
			'general'  => __( 'General', 'siteripper' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'siteripper_registered_settings_sections', 'siteripper_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function siteripper_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'siteripper_unsavable_tabs', 'siteripper_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $sections The updated sections
 */
function siteripper_define_unsavable_sections() {
	$sections = array( 'ripper/dashboard' );

	return $sections;
}
add_filter( 'siteripper_unsavable_sections', 'siteripper_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function siteripper_registered_settings( $settings ) {
	$dashboard_settings = array(
		'dashboard'    => apply_filters(
			'siteripper_registered_settings_dashboard',
			array(
				'dashboard' => array(
					array(
						'id'   => 'dashboard_header',
						'name' => '<h2>' . __( 'Dashboard', 'siteripper' ) . '</h2>',
						'desc' => '',
						'type' => 'header',
					),
				),
			),
		),
	);

	$dashboard_settings = apply_filters( 'siteripper_registered_settings_dashboard', $dashboard_settings );

	$support_settings = array(
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'SiteRipper Support', 'siteripper' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'siteripper' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $dashboard_settings, $support_settings );
}
add_filter( 'siteripper_registered_settings', 'siteripper_registered_settings' );