<?php
/**
 * Scripts
 *
 * @package     WidgitLabs\SiteRipper\Scripts
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Load admin scripts
 *
 * @since       1.0.0
 * @return      void
 */
function siteripper_admin_enqueue_scripts() {
	// Use minified libraries if SCRIPT_DEBUG is turned off.
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	// Setup versioning for internal assets.
	$css_ver = gmdate( 'ymd-Gis', filemtime( SITERIPPER_DIR . 'assets/css/admin' . $suffix . '.css' ) );
    // $js_ver  = gmdate( 'ymd-Gis', filemtime( SITERIPPER_DIR . 'assets/js/admin' . $suffix . '.js' ) );

	wp_register_style( 'siteripper', SITERIPPER_URL . 'assets/css/admin' . $suffix . '.css', array(), SITERIPPER_VER . '-' . $css_ver );
	wp_enqueue_style( 'siteripper' );

    // wp_enqueue_script( 'siteripper', SITERIPPER_URL . 'assets/js/admin' . $suffix . '.js', array( 'jquery' ), SITERIPPER_VER . '-' . $js_ver, true );
}
add_action( 'admin_enqueue_scripts', 'siteripper_admin_enqueue_scripts' );
