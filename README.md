# SiteRipper

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wordpress/siteripper/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wordpress/siteripper/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wordpress/siteripper/pipelines)

A generic content ripper based on pattern matching.

## Description

Coming soon?

## Installation

SiteRipper is specifically designed to work in any WordPress-based environment.
Once downloaded, extract the `siteripper` directory, upload it to your
`wp-content/plugins` directory, and activate the plugin.

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/siteripper/issues)!
