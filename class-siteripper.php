<?php
/**
 * Plugin Name:     SiteRipper
 * Plugin URI:      https://alphaaweb.com
 * Description:     A generic content ripper based on pattern matching.
 * Author:          Widgit Labs
 * Author URI:      https://widgitlabs.com
 * Version:         1.0.0
 * Text Domain:     siteripper
 * Domain Path:     languages
 *
 * @package         WidgitLabs\SiteRipper
 * @author          Daniel J Griffiths <dan@apppresser.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'SiteRipper' ) ) {

	/**
	 * Main SiteRipper class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class SiteRipper {


		/**
		 * The one true SiteRipper
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         SiteRipper $instance The one true SiteRipper.
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object.
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true SiteRipper.
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof SiteRipper ) ) {
				self::$instance = new SiteRipper();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'siteripper' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'siteripper' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'SITERIPPER_VER' ) ) {
				define( 'SITERIPPER_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'SITERIPPER_DIR' ) ) {
				define( 'SITERIPPER_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'SITERIPPER_URL' ) ) {
				define( 'SITERIPPER_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'SITERIPPER_FILE' ) ) {
				define( 'SITERIPPER_FILE', str_replace( plugin_dir_path( __FILE__ ), 'siteripper/', __FILE__ ) );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $siteripper_options;

			// Conditionally load the settings library.
			if ( ! class_exists( 'Origami' ) && file_exists( SITERIPPER_DIR . 'vendor/widgitlabs/origami/class-origami.php' ) ) {
				require_once SITERIPPER_DIR . 'vendor/widgitlabs/origami/class-origami.php';
			}

			require_once SITERIPPER_DIR . 'includes/admin/settings/register.php';

			// Ensure we have access to is_plugin_active_for_network.
			if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}

			$location = is_plugin_active_for_network( SITERIPPER_FILE ) ? 'network' : 'site';

			self::$instance->settings = new Origami( 'siteripper', 'ripper', array( 'sysinfo' ), $location );
			$siteripper_options       = self::$instance->settings->get_settings();

			require_once SITERIPPER_DIR . 'includes/scripts.php';

			// if ( defined( 'WP_CLI' ) && WP_CLI ) {
	            // require_once SITERIPPER_DIR . 'includes/class-siteripper-cli.php';
			// }
			// require_once SITERIPPER_DIR . 'includes/helper-functions.php';
			

			// if ( is_admin() ) {
			// 	require_once SITERIPPER_DIR . 'includes/admin/meta-boxes.php';
			// }
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'siteripper_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'siteripper' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'siteripper', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/siteripper/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/siteripper/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/siteripper folder.
				load_textdomain( 'siteripper', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/siteripper/languages/ folder.
				load_textdomain( 'siteripper', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/siteripper/ folder.
				load_textdomain( 'siteripper', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'siteripper', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true SiteRipper
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $siteripper = siteripper(); ?>
 *
 * @since       1.0.0
 * @return      SiteRipper The one true SiteRipper
 */
function siteripper() {
	return SiteRipper::instance();
}

// Get things started.
siteripper();
